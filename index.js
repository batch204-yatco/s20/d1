// console.log("Hello World!");

/*
	Repetition Control Structure (Loops)
		- Loops are one of the most important features that programming must have
		- Lets us execute a code repeatedly in a pre-set number or forever
*/

/*
	Mini Activity:
		Create function named greeting and display "Hello, my world" using console.log inside function.

		Invoke the greeting() function 20 times
*/

function greeting() {
	console.log("Hello, my World!")
}

let count = 20;

while (count !== 0) {
	console.log("This is printed inside the loop: " + count);
	greeting();

	count--;
}

/*
	While loop - takes in expression/condition

	Syntax:
		while(expression/condition)
			statement
*/

/*
	Mini Activity:
		While loop should only display number 1-5
		Correct the following loop

		Let x = 0;

		while(x < 1){
			console.log(x);
			x--;
		}
*/

let x = 1;

while(x !== 6){
	console.log(x);
	x++;
}

/*
	Do While Loop
		- Do-while loop works like while loop but unlike while loops, it guarantees code will be executed at least once

	Syntax:
		do{
			statement
		} while (expression/condition)
*/

// let number = Number(prompt("Give me a number: "));
// do{
// 	console.log("Do While: "+ number);

// 	number += 1;
// } while(number < 10);

/*
	FOR LOOPS
		- more flexible than while and do while loop

	Syntax:
		for(initialization; expression/condition; finalExpression){
			statement 
		}
*/

let number = Number(prompt("Give me a number:"));

for (let count = 0; count <= 20; count++){
	console.log("For Loop: " + count)
}

// Can we use for loops in string?
let myString = "eric";

// Accessing elements of string
// Index number to access specific in string
console.log(myString[0]);//e
console.log(myString[1]);//r

console.log(myString.length);//number of characters

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

// 

let myName = "alExandrO";

for (let i = 0; i < myName.length; i++) {

	if(
		myName[i].toLowerCase() === "a" ||
		myName[i].toLowerCase() === "e" ||
		myName[i].toLowerCase() === "i" ||
		myName[i].toLowerCase() === "o" ||
		myName[i].toLowerCase() === "u"  
	) {
		console.log("Vowel!");

	} else {
		
		console.log(myName[i]);
	}
}

// Continue and Break Statements
/*
	"Continue" statement allows code to go to next iteration of loop without finishing the execution of all statements in a code block

	"Break" statement is used to terminate current loop once match has been found

*/

for (let count = 0; count <= 20; count++){

	if(count % 2 === 0){
		console.log("Even Number");
		continue; //skips the statement below
	}

	console.log("Continue and Break: "+count);
	if(count > 10){
		break;
	}
}

let name = "alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i].toLowerCase() === "d"){
		break;
	}

	
}